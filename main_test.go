package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	initialMsg = "b4759e820cb549c53c755e5905c744f73605f8f6437ae7884252a5f204c8c6e6"
	merkleRoot = "f832e7458a6140ef22c6bc1743f09610281f66a1b202e7b4d278b83de55ef58c"
)

func TestReverseArray(t *testing.T) {

	result := ReverseArray([]byte{1, 2, 3})

	assert.Equal(t, []byte{0x3, 0x2, 0x1}, result)
}

func TestMapping(t *testing.T) {
	tuples := [][]string{{"sha256", "5a732436", "9d516c058e000e98"}}
	timestamps := MapTimestampsFromTuples(tuples)

	assert.Equal(t, []Timestamp{{Operator: "sha256", Prefix: "5a732436", Postfix: "9d516c058e000e98"}}, timestamps)
}

func TestFirstMessageShouldResultInComparableHash(t *testing.T) {

	var timestamps []Timestamp = []Timestamp{{Operator: "sha256", Prefix: "5a732436", Postfix: "9d516c058e000e98"}}

	merkleRoot = ReverseStringOrder("0aaab62544b1799c0bab167d4ee6a74584563afde95a4e90692dc5ded01e6e58")

	res := VerifyHash(timestamps, initialMsg, merkleRoot)

	assert.True(t, res)
}
