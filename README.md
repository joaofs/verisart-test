# Verisart Coding Test

This coding test is about using standard Go/Python routines to parse a Merkle path embedded in a json file. A Merkle
path consists of a series of cryptographic operations, applied to an initial `message`. Each operation has an `Operator` and
two operands (`Prefix` and `Postfix`). The output of each operation is `Operator(Prefix + message + Postfix)`, which will become
the `message` in the operation. The final `message` is a hash called the Merkel root. Your task is to implement a
function called `VerifyHash` that takes in a list of operations, the initial message and the Merkle root and returns whether
the message can be verified by following the path.

Please use the provided files (main.go or main.py) as the starting points.

# Implementation notes

- Added a mapping function to convert the timestamp tuples in Timestamp struct
- Wrote a reverse byte array function in order to revert byte order
- Looped trough the timestamp array so that I could calculate the compounding hash for the Merkle root
- In the end reverted byte order to be able to compare the provided Merkle root with the message from the resulting operations since they had different endianness

# Quick start

- go run main.go
- go test
