package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Timestamp struct {
	Operator string // [0]
	Prefix   string // [1]
	Postfix  string // [2]
}

//ReverseArray ...
func ReverseArray(originalArray []byte) []byte {

	var reversedMessageInBytes []byte
	reversedMessageInBytes = make([]byte, len(originalArray), cap(originalArray))
	j := 0
	for i := len(originalArray) - 1; i >= 0; i-- {

		reversedMessageInBytes[j] = originalArray[i]
		j++
	}

	return reversedMessageInBytes
}

//ReverseStringOrder ...
func ReverseStringOrder(str string) string {
	strInBytes, err := hex.DecodeString(str)
	if err != nil {
		fmt.Printf("issue decoding string")
	}
	strInBytesRev := ReverseArray(strInBytes)
	strRev := hex.EncodeToString(strInBytesRev)

	return strRev
}

//Operation ...
func Operation(message string) string {
	messageInBytes, err := hex.DecodeString(message)

	if err != nil {
		fmt.Printf("issue decoding string")
	}

	h := sha256.New()
	h.Write(messageInBytes)

	prtSlice := hex.EncodeToString(h.Sum(nil))

	return prtSlice
}

// This function should walk through the timestamps and verify message against merkleRoot
// Hints: use crypto/sha256 and encoding/hex. message is big-endian while merkleRoot is little-endian.
func VerifyHash(timestamps []Timestamp, message string, merkleRoot string) bool {

	var concatMsg string
	resultMessage := message

	for i, timestamp := range timestamps {
		i++
		concatMsg = timestamp.Prefix + resultMessage + timestamp.Postfix

		resultMessage = Operation(concatMsg)
	}

	merkleRootRev := ReverseStringOrder(merkleRoot)

	return resultMessage == merkleRootRev
}

//MapTimestampsFromTuples ...
func MapTimestampsFromTuples(tuples [][]string) []Timestamp {
	timestamps := make([]Timestamp, len(tuples), cap(tuples))

	for i, tuple := range tuples {
		timestamps[i].Operator = tuple[0]
		timestamps[i].Prefix = tuple[1]
		timestamps[i].Postfix = tuple[2]
	}

	return timestamps
}

func main() {
	msg := "b4759e820cb549c53c755e5905c744f73605f8f6437ae7884252a5f204c8c6e6"
	merkleRoot := "f832e7458a6140ef22c6bc1743f09610281f66a1b202e7b4d278b83de55ef58c"

	filePath := "./bag/timestamp.json"
	dat, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	tuples := [][]string{}
	timestamps := []Timestamp{}

	// Convert byte slice to slice of tuples
	json.Unmarshal(dat, &tuples)

	timestamps = MapTimestampsFromTuples(tuples)

	if VerifyHash(timestamps, msg, merkleRoot) == true {
		fmt.Println("CORRECT!")
	} else {
		fmt.Println("INCORRECT!")
	}
}
